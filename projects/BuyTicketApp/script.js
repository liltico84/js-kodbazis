let quantity = prompt('How many tickets would you buy?');
let TicketType = prompt('What kind of ticket would you like? Student, Adult or Pensioner?');
let TicketTypes = {
    'Student' : 320,
    'Adult' : 350,
    'Retired' : 250
}
let price = TicketTypes[TicketType];
if (price === undefined) {
    alert('Invalid ticket type!');
} else {
    let discount = quantity > 10 ? 0.9 : 1;
    let sumResult = quantity * price * discount;
    alert(sumResult);
}

