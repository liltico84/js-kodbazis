let text1 = `Hello Jason`;
let text2 = `Hello Mrs Johnson's cat`;
let text3 =
    `This is a
    funny-bunny cat!`;
let firstname = 'Martha';
let lastname = 'Stuart';
let text4 = `Hello ${firstname} ${lastname}`;
let value1 = 10;
let value2 = 20;
let value3 = value1 + value2;
let addValue = `Összeg: ${value1} + ${value2} azaz ${value3}`;

document.getElementById('template1').innerHTML = text1;
document.getElementById('template2').innerHTML = text2;
document.getElementById('template3').innerHTML = text3;
document.getElementById('template4').innerHTML = text4;
document.getElementById('template5').innerHTML = addValue;

let header = 'My favourite classical song writers';
let tags = ['Strauss', 'Mozart', 'Beethowen'];
let html = `<h2>${header}</h2><ul>`
for (let x of tags) {
    html += `<li>${x}</li>`;
}
html +=`</ul>`;
document.getElementById('template6').innerHTML = html;