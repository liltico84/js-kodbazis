let state = {
  products: [
    {
      id: uuidv4(),
      name: 'Gorenje 500',
      price: 25000,
      isInStock: true
    },
    {
      id: uuidv4(),
      name: 'Acer 1200',
      price: 380000,
      isInStock: false
    },
    {
      id: uuidv4(),
      name: 'Asus T600',
      price: 540000,
      isInStock: true
    }
  ],
  editId: ''
}

function renderEditProduct() {
  if(state.editId === ''){
    document.getElementById('editProduct').innerHTML = '';
    return;
  }

  let foundProduct;
  for (let product of state.products) {
    if(product.id === state.editId) {
      foundProduct = product;
      break;
    }
  }
  let editFormHTML = `
    <form id="editForm">
      <h3>Edit Product</h3>
      <label for="productName">Product's name: </label>
      <input type="text" id="productName" value="${foundProduct.name}"><br><br>
      <label for="productPrice">Product's price:</label>
      <input type="text" id="productPrice" value="${foundProduct.price}"><br><br>
      <label for="productStock">Is in stock?</label>
      <input type="checkbox" id="productStock" ${foundProduct.isInStock ? 'checked' : '' }>
      <button type="submit">Send</button>
    </form>
  `;
  document.getElementById('editProduct').innerHTML = editFormHTML;
  document.getElementById('editForm').onsubmit = function (event) {
    event.preventDefault();
    let newName = event.target.elements.productName.value;
    let newPrice = Number(event.target.elements.productPrice.value);
    let newStock = event.target.elements.productStock.checked;

    let foundIndex;
    for(let index = 0; index < state.products.length; index++) {
      if(state.products[index].id === state.editId) {
        foundIndex = index; // megkapjuk az adott kártya indexét, 0..1..2..stb
        break;
      }
    }
    state.products[foundIndex] = {
      id : state.editId,
      name: newName,
      price: newPrice,
      isInStock: newStock
    };
    state.editId = '';
    renderFromState();
    renderEditProduct();
  }
}
function renderFromState () {
  let productTemplate = '';
    for (let product of state.products) {
      productTemplate += `
        <div class="template-block ${product.isInStock ? '' : 'red'}">
          <p>Name: ${product.name}</p>
          <p>Price: ${product.price}</p>
          <button class="yellow editProduct" data-productid="${product.id}">Edit</button>
          <button class="red deleteProduct" data-productid="${product.id}">Delete</button>
        </div>
      `;
    }
  document.getElementById('product-list-component').innerHTML = productTemplate;

  for(let editBtn of document.querySelectorAll('.editProduct')){
    editBtn.onclick = function (event) {
      let id = event.target.dataset.productid;
      state.editId = id;
      renderEditProduct();
    }
  }

  for(let deleteBtn of document.querySelectorAll('.deleteProduct')){
    deleteBtn.onclick = function (event) {
      let id = event.target.dataset.productid; // a gomb megnyomására generált random id
      let foundIndex;
      for(let index = 0; index < state.products.length; index++) {
        if(state.products[index].id === id) {
          foundIndex = index; // megkapjuk az adott kártya indexét, 0..1..2..stb
          break;
        }
      }
      state.products.splice(foundIndex, 1);
      renderFromState();
    }
  }
}
window.onload = renderFromState;
document.getElementById('miniWebShop').onsubmit = function (event){
  event.preventDefault();
  let newName = event.target.elements.productName.value;
  let newPrice = Number(event.target.elements.productPrice.value);
  let newStock = event.target.elements.productStock.checked;
  state.products.push({
    id: uuidv4(),
    name: newName,
    price: newPrice,
    isInStock: newStock
  });
  renderFromState();
}
function uuidv4() {
  return "10000000-1000-4000-8000-100000000000".replace(/[018]/g, c =>
      (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
  );
}

