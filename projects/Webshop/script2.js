let state = {
    products: [
        {
            name: 'apple',
            price: 241,
            isInStock: false
        },
        {
            name: 'banana',
            price: 7800,
            isInStock: true
        },
        {
            name: 'pineapple',
            price: 5500,
            isInStock: true
        }
    ]
}
function render(){
    let productTemplate = '';
    for (let product of state.products){
        productTemplate += `
            <div class="template-block ${product.isInStock ? '' : 'red'}">
                <p>Name: ${product.name}</p>
                <p>Price: ${product.price}</p>
                <p>Is in stock: ${product.isInStock}</p>
                <button>Edit</button>
                <button>Delete</button>
            </div> 
        `;
    }
    document.getElementById('product-list-component').innerHTML = productTemplate;
}
window.onload = render;