let numbers = [3, 4, 8, 10, 12, 16];
let oddNumbers = 0;
for (let number of numbers) {
    oddNumbers += number % 2 !== 0 ? 1: 0;
}
console.log(oddNumbers);