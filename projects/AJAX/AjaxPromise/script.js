document.getElementById('login').onclick = function () {
    let url = 'https://reqres.in/api/login';
    let body = JSON.stringify({
        email: 'eve.holt@reqres.in',
        password: 'cityslicka'
    });
    sendRequest2(url, 'POST', body)
        .then(function (response) {
            console.log(response);
            return sendRequest2('https://reqres.in/api/users', 'GET', null)
        })
        .then(function (futureValue) {
            console.log(futureValue);
            return sendRequest2('https://reqres.in/ai/users', 'GET', null) //elrontott url, ami miatt hibára fut, azaz reject
        })
        .catch(function (error) {
            console.log(error);
        })
}
//promise - itt már a callback function nincs jelen. Azt mondjuk meg mikor minősül a kérés sikeresnek vagy sikertelennek.
function sendRequest2(url, method, body) {
    return new Promise(function(resolve, reject){
        let xhr = new XMLHttpRequest;
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
                if(xhr.status >= 200 && xhr.status < 300) {
                    resolve(JSON.parse(xhr.responseText));
                } else {
                    reject(xhr.responseText);
                }
            }
        }
        xhr.open(method, url);
        xhr.setRequestHeader('content-type', 'application/json'); // header információ bekötése
        xhr.send(body);
    });
}