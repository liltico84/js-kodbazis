let userInput = prompt("How many eggs would you like to make scrambled eggs from?");
eggQuantity();
frying();

function eggQuantity() {
    console.log('Scrambled eggs from ' + userInput + ' eggs');
    console.log('---------');
}

function frying(){
    console.log('- Pour a little oil into the pan.');
    addEggs (userInput);
    addSpices();
    console.log('- Stir and fry for 4 minutes,');
    console.log('  and also done!');
    enjoy();
}

function addEggs(eggNumber) {
    console.log('- Add '+ eggNumber + ' beaten eggs.');
}

function addSpices() {
    console.log('- Add a little salt,');
    console.log('  a little pepper,');
    console.log('  and a little grounded red paprika.');
}

function enjoy() {
    console.log('------------');
    console.log('Enjoy your meal!');
}