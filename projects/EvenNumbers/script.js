let numbers = [2, 7, 9, 3, 15, 21];
let evenNumbers = 0;
for (let index = 0; index<numbers.length; index++) {
    evenNumbers += numbers[index] % 2 === 0 ? 1 : 0;
}
console.log(evenNumbers);