// 1. box - be blur or not depends on mouse click event

let isBlurred = false;
document.getElementById('element-one').onclick = function () {
    isBlurred = !isBlurred;
    if (isBlurred) {
        document.getElementById('element-one').classList.add ('blur');
    } else {
        document.getElementById('element-one').classList.remove ('blur');
    }
}

// 2. box - be red or not depends on mouse hover or mouse out events

let isRed = false;
document.getElementById('element-two').onmouseover = function () {
    isRed = !isRed;
    if (isRed) {
        document.getElementById('element-two').classList.add('red');
        document.getElementById('element-two').onmouseout = function () {
            isRed = !isRed;
            document.getElementById('element-two').classList.remove('red');
        }
    }
}

// 3. box - generate a random number from 1 to 20 depends on double mouse click event

document.getElementById('element-three').ondblclick = function () {
    let generateNumber = randomNumber(1, 20);
    document.getElementById('element-three').firstElementChild.innerHTML = String(generateNumber); //generateNumber.toString();
}
function randomNumber(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}

// 4. box - box is hidden if user click mouse and after 1 second appears again

document.getElementById('element-four').onclick = function () {
    hiddenObject();
    setTimeout(appearObject, 1000);
};
function hiddenObject (){
    document.getElementById('element-four').classList.add('hidden');
}
function appearObject (){
    document.getElementById('element-four').classList.remove('hidden');
}

// 5. box - all boxes to be round depends on mouse click event

let boxes = document.querySelector('.container').children;
document.getElementById('element-five').onclick = function () {
    for (let box of boxes) {
        box.classList.toggle('circle');
    }
}

// 6. box - the user add an input data that appears as the box's content - submit event
document.getElementById('box6').onsubmit = function (event) {
    event.preventDefault();
    document.getElementById('element-six').firstElementChild.innerHTML = event.target.elements.boxnumber.value;
}

// 7. box - the user add an input data that appears as the box's content - keypress event

document.getElementById('box7').onkeypress = function (event) {
    document.getElementById('element-seven').firstElementChild.innerHTML = event.key;
}

// 8. box - mouse's coordinates appear in the box depends on mouse movement

document.onmousemove = function (event) {
    document.getElementById('element-eight').firstElementChild.innerHTML = 'X: ' + event.clientX + ', Y: ' + event.clientY;
}

// 9. box - build a mini calculator
let state = Number(document.getElementById('element-nine').firstElementChild.innerText);
document.getElementById('box9').onsubmit = function (event) {
    event.preventDefault();
    let operator = event.target.elements.operator.value;
    let addNumber = Number(event.target.elements.operand.value);
    switch (operator) {
        case 'mult':
            state *= addNumber;
            break;
        case 'div':
            state /= addNumber;
            break;
        case 'sub':
            state -= addNumber;
            break;
        case 'add':
            state += addNumber;
            break;
    }
    document.getElementById('element-nine').firstElementChild.innerHTML = state;
}
