// 1. box - be blur or not depends on mouse click event
let isBlurred = false;
document.getElementById("element-one").onclick = function () {
    isBlurred = !isBlurred;
    if (isBlurred) {
        document.getElementById("element-one").classList.add("blur");
    } else {
        document.getElementById("element-one").classList.remove("blur");
    }
}

// 2. box - be red or not depends on mouse hover or mouse out events

let isHover = false;
document.getElementById("element-two").onmouseover = function () {
    isHover = true;
    render();
}
document.getElementById("element-two").onmouseout = function () {
    isHover = false;
    render();
}
function render() {
    if (isHover) {
        document.getElementById("element-two").classList.add("red");
        //document.getElementById("element-two").style.backgroundColor = "red";
    } else {
        document.getElementById("element-two").classList.remove("red");
        //document.getElementById("element-two").style.backgroundColor = "";
    }
}

// 3. box - generate a random number from 1 to 20 depends on double mouse click event

document.getElementById("element-three").ondblclick = function () {
    document.getElementById("element-three").firstElementChild.innerHTML = randomNumber(1,20);
}
function randomNumber(min, max){
    return Math.floor(Math.random() * (max-min) + min);
}

// 4. box - box is hidden if user click mouse and after 1 second appears again

document.getElementById("element-four").onclick = function () {
    document.getElementById("element-four").classList.add("hidden");
    //document.getElementById("element-four").style.display = "none";
    setTimeout(hiddenOne,1000);
}
function hiddenOne() {
    document.getElementById("element-four").classList.remove("hidden");
    //document.getElementById("element-four").style.display = "block";
}

// 5. box - all boxes will be circle if the user click with the mouse

let isCircle = false;
let arrayShapes = document.getElementsByClassName("shape");
document.getElementById("element-five").onclick = function () {
    isCircle = !isCircle;
    if (isCircle) {
        for (let index = 0; arrayShapes.length; index++ ) {
            arrayShapes[index].classList.add("circle");
        }
    } else {
        for ( let index = 0; arrayShapes.length; index++) {
            arrayShapes[index].classList.remove("circle");
        }
    }
}

// 6. box - the user add an input data that appears as the box's content

document.getElementById('box6').onsubmit = function (event) {
    event.preventDefault(); // with this command, we damage to send the base HTML request because of the submit event
    document.getElementById('element-six').firstElementChild.innerHTML = event.target.elements.boxnumber.value;
}

// 7. box - the user add an input data that appears as the box's content - key press event

document.getElementById('box7').onkeypress = function (event) {
    document.getElementById('element-seven').firstElementChild.innerHTML = event.key;
}

// 8. box - mouse's coordinates appear in the box depends on mouse movement

document.onmousemove = function (event) {
    let coordinates = 'X: ' + event.clientX + ' Y: ' + event.clientY;
    document.getElementById('element-eight').firstElementChild.innerHTML = coordinates;
}

// 9. box - build a mini calculator

let state = 9;
document.getElementById('element-nine').onsubmit = function (event) {
    event.preventDefault();
    let operand = Number(event.target.elements.operand.value);
    let operator = event.target.elements.operator.value;
    switch (operator) {
        case "mult":
            state = state * operand;
        break;
        case "div":
            state = state / operand;
        break;
        case "add":
            state = state + operand;
        break;
        case "sub":
            state = state - operand;
        break;
    }
    document.getElementById('element-nine').firstElementChild.innerHTML = state;
}