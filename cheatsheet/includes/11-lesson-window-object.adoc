== 11. Window object

=== 11. 1 Definíció

Window object - reprezentálja a böngészőablakot, mint objektumot. Kulcs-érték párok sorozata.

image:../assets/11_WindowObject.JPG[]

NOTE: window.console.log('Hello); - a window elhagyható, mivel ez egy globális objektum

NOTE: legfontosabb eleme a Window object-nek a document, mert ezzel lehet a HTML oldalt vezérelni.

Lehetőség van a window objektum valamely elementjének JS elérésére is, amivel még interaktívabbá tudjuk tenni az oldalt.

** Jobb klikk az elemre / copy property path
** document.all[5].style.backgroundColor = "red";

[source, javascript]
----
// beégetett elemről van szó a példában, ami nem ad dinamizmust
document.all[5].onclick = function() {
  console.log('ok');
}
----

Window object elem változtatása során szükségesek:

* az elem JS reprezentációja

* milyen esemény a reakció indítója

* milyen kódot akarunk futtatni

<<<