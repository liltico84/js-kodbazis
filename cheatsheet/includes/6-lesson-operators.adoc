== 6. Operátorok

=== 6. 1 Definíciók

*Operátor*: Adaton műveletet végrehajtó nyelvi eszköz

*Csoportosíthatók összetett kifejezésekben*

[source, javascript]
----
console.log(2 === 2 && 1 > 5); //false
----

*Végrehajtási sorrend, prioritás*

* && előbb hajtódik végre mint az ||
* () használatával felül lehet írni a sorrendet
* szorzás előbb van mint az összeadás

*Mellékhatás - side effect*: Az alap metódus végrehajtása mellett a rendszer működésére is hatással van.

* kiírhat a konzolba
* beolvashat valahonnan adatot
* eltárolhat adatot a memóriába
* nem determinisztikusan működik, azaz ha többször meghívod, nem mindig ugyanazt az értéket adja vissza

=== 6. 2 - Operátor csoportosítások

* [.underline]#Operandusok száma szerint#
** 1: unary - egy operanduson dolgozik
** 2: binary - 3 + 5
** 3: ternary - conditional

* [.underline]#Operátor elhelyezkedése az operandusokhoz képest#
** előtte: prefix
** közötte: infix
** utána: postfix

* [.underline]#Hajt-e végre mellékhatást?#
** igen
** nem - ugyanazon inputot kapva, ugyanazon outputot generálják ki, vagyis 2 + 3 mindig 5 lesz.

=== 6. 3 Operátor családok - nincs mellékhatás

* [.underline]#Aritmetikai# : Bináris, infix és nincs mellékhatása
** +
** -
** *
** /
** % - modulus (osztás után kapott maradék)

* [.underline]#Összehasonlító# : többféle típus lehet a bemenet, a kimenet csak boolean.

** ==  egyenlőség - equality
** != nem egyenlő
** === szigorú egyenlőség - identity
** !== szigorú nem egyenlőség
** >
** <
** > =
** < =

NOTE: 2 == 2 - igaz, 2 == '2' - igaz - a nyelv gyenge típusossága, a JavaScript típus átalakítást végez ennél az operátornál.

NOTE: 2 === '2' - hamis, mivel itt nem hajt végre típus átalakítást.

* [.underline]#Logikai# : a bemenet és a kimenet is boolean típusú

** && - logikai ÉS - csak akkor igaz, ha mindkét bemenet igaz
** || - logikai VAGY - akkor igaz, ha az egyik bemenet igaz
** !érték - negálás - az értéknek az ellenkezőjét adja meg

* [.underline]#String operátor# : konkatenálás
** +

* [.underline]#Conditional operátor# : egyetlen ternery
** ? :

NOTE: variablename = (condition) ? value1 : value2;

[source, javascript]
----
console.log(true ? 'Hello' : 'Viszlát'); //Hello
console.log(false ? 'Hello' : 'Viszlát'); //Viszlát
----

=== 6. 4 Operátor családok - van mellékhatás

* [.underline]#Assignment# : memóriában tárol el értéket változó segítségével

NOTE: váltózó: memóriában lévő kulcs, melyhez értéket fűzünk

NOTE: a JavaScript dinamikus, típusos nyelv. Ez annyit jelent, hogy egy változó többféle értéket is felvehet a program futása során. Statikus nyelveknél ez sokkal szigorúbb.

[source, javascript]
----
var valtozo = 20;
console.log(valtozo); //20
----

* [.underline]#Increment, Decrement#

** ++ (increment)
** - - (decrement)

* [.underline]#Bővített aritmetikai operátorok#

** += (adott értékkel növeli a változó értékét)
** -= (adott értékkel csökkenti a változó értékét)
** *= (adott értékkel megszorozza a változó értékét)
** /= (adott értékkel osztja a változó értékét)
** %= (modulus értékével módosítja a változó értékét)

<<<